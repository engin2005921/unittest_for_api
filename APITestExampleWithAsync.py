import datetime
import json
import requests
import unittest
import asyncio

from dateutil.parser import parse


class APITestExample(unittest.TestCase):
    def setUp(self):
        self.foo = 'bar'
        self.url = 'https://www.google.com.tw'
        self.loop = asyncio.get_event_loop()

    def tearDown(self) -> None:
        self.foo = None
        self.url = None
        self.loop = None

    def request_builder(self):
        result = requests.get(self.url)
        return result

    async def send_request(self):
        res = await self.loop.run_in_executor(None, self.request_builder)
        print('request url: ', res.url)
        print('request status_code: ', res.status_code)
        print('request text: ', res.text[:100])  # the response data can be parsed to json if needed.

    def test_foo_ten_times_async(self):
        tasks = []

        for i in range(2):
            tasks.append(self.loop.create_task(self.send_request()))

        self.loop.run_until_complete(asyncio.wait(tasks))
