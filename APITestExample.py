import datetime
import json
import requests
import unittest
from dateutil.parser import parse


class APITestExample(unittest.TestCase):
    def setUp(self):
        self.foo = 'bar'
        self.url = 'https://apiservice.mol.gov.tw/OdService/download/A17000000J-020044-HYO'
        self.result = requests.get(self.url, verify=False)

    def tearDown(self) -> None:
        self.foo = None
        self.url = None

    def date_validator(self, param):
        try:
            parse(param)
            return True
        except ValueError:
            return False

    def test_foo(self):

        # confirm status code = 200
        self.assertTrue(self.result.status_code == 200)

        # confirm the schema is [{}, {}, ....]
        parsed_result = json.loads(self.result.text)
        print(parsed_result)
        self.assertTrue(type(parsed_result) is list)
        self.assertTrue(len(parsed_result) > 0)
        self.assertTrue(type(parsed_result[0]) is dict)

        # confirm the keys are valid
        self.assertIn('年月別', parsed_result[0].keys())
        self.assertIn('最近月份收益率', parsed_result[0].keys())
        self.assertIn('公告日期', parsed_result[0].keys())

        # confirm the value are in correct type
        self.assertEqual(type(parsed_result[0].get('年月別')), str)
        self.assertTrue(self.date_validator(parsed_result[0].get('年月別')))

        self.assertEqual(type(parsed_result[0].get('最近月份收益率')), str)

        self.assertEqual(type(parsed_result[0].get('公告日期')), str)
        self.assertTrue(self.date_validator(parsed_result[0].get('公告日期')))
